package com.nCinga.bo;

public class IssuedBook {

    private Student student;
    private Admin admin;
    private Book book;

    private boolean bookReturn = false;

    public IssuedBook(Student student,Admin admin,Book book){
        setStudent(student);
        setAdmin(admin);
        setBook(book);
    }

    public void setBookReturn(boolean bookReturn) {
        this.bookReturn = bookReturn;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Admin getAdmin() {
        return admin;
    }

    public Book getBook() {
        return book;
    }

    public Student getStudent() {
        return student;
    }
}
