package com.nCinga.bo;

public class Book {

    private String name;
    private Subject subject;
    private int totalCount = 0;
    private int issuedBookCount = 0;


    public Book(String name, Subject subject, int totalCount, int issuedBookCount) {
        setIssuedBookCount(issuedBookCount);
        setTotalCount(totalCount);
        setName(name);
        setSubject(subject);
    }

    public void increaseIssuedBookCount(){
        setIssuedBookCount(this.issuedBookCount+1);
    }

    public void decreaseIssuedBookCount(){
        setIssuedBookCount(this.issuedBookCount-1);
    }

    public void setIssuedBookCount(int issuedBookCount) {
        if (issuedBookCount >= 0)
            this.issuedBookCount = issuedBookCount;
        else
            this.issuedBookCount = 0;
    }

    public void setTotalCount(int totalCount) {

        if (totalCount >= 0)
            this.totalCount = totalCount;
        else
            this.totalCount = 0;
    }

    public int getIssuedBookCount() {
        return issuedBookCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public Subject getSubject() {
        return subject;
    }
}
