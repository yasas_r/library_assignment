package com.nCinga.bo;

import com.nCinga.Exception.CantIssueBookException;
import com.nCinga.Exception.CantIssuedDegreeException;
import com.nCinga.Exception.CantReachMoreThanTotalException;

import java.util.Objects;

public class Library {

    private Book[] books;


    public Library(Book[] books) {
        setBooks(books);
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }

    public Book[] getBooks() {
        return books;
    }

    public boolean isThereBook(Book book){
        for(int i=0; i < this.books.length ; i++){
            if(book.getName()==this.books[i].getName()){
                return true;
            }
        }
        return false;
    }

    public void issueBookService(Student student, Admin admin, Book book, IssuedBook issuedBook) {

        int avalableBookCount = book.getTotalCount() - book.getIssuedBookCount();
        if (avalableBookCount == 0) {
            throw new CantIssueBookException("Book is not availabel");
        } else if (student.getDegree() == Degree.B_TECH && student.getIssuedBookStudentCount() >= 5) {

            throw new CantIssuedDegreeException("Cant issued more than 5 books for B-tech students");
        } else if (student.getDegree() == Degree.M_TECH && student.getIssuedBookStudentCount() >= 10) {

            throw new CantIssuedDegreeException("Cant issued more than 5 books for B-tech students");
        } else {
            issuedBook = new IssuedBook(student, admin, book);
            student.increaseIssuedBook();
            book.increaseIssuedBookCount();
            System.out.println("Book is issued "+ book.getIssuedBookCount());
        }
    }

    public void returnBookService(Student student, Admin admin, Book book, IssuedBook issuedBook) {

        int avalableBookCount = book.getTotalCount() - book.getIssuedBookCount();
        if (!Objects.equals(student, issuedBook.getStudent())) {
            System.out.println("You didn't issue this book");
        } else if (book.getTotalCount() < avalableBookCount + 1) {
            throw new CantReachMoreThanTotalException();
        } else {
            issuedBook.setBookReturn(true);
            book.decreaseIssuedBookCount();
            System.out.println("Book is returned "+ book.getIssuedBookCount());
        }
    }



}
