package com.nCinga.bo;

public class Student {

    private int rollNo;
    private String name;
    private String batch;
    private String section;
    private Degree degree;

    private int issuedBookStudentCount;

    public Student(int rollNo,String name,String batch,String section,Degree degree,int issuedCount){
        setRollNo(rollNo);
        setBatch(batch);
        setSection(section);
        setDegree(degree);
        setIssuedBookStudentCount(issuedCount);

    }

    public void increaseIssuedBook(){
        setIssuedBookStudentCount(this.issuedBookStudentCount+1);
    }

    public int getIssuedBookStudentCount() {
        return issuedBookStudentCount;
    }

    public  void setIssuedBookStudentCount(int issuedBookStudentCount) {
        this.issuedBookStudentCount = issuedBookStudentCount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public void setDegree(Degree degree) {
        this.degree = degree;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSection() {
        return section;
    }

    public String getName() {
        return name;
    }

    public int getRollNo() {
        return rollNo;
    }

    public String getBatch() {
        return batch;
    }

    public Degree getDegree() {
        return degree;
    }
}
