package com.nCinga.bo;

public class Admin {

    private String name;
    private int idNo;

    public Admin(String name, int idNo){
        setName(name);
        setIdNo(idNo);

    }

    public void setIdNo(int idNo) {
        this.idNo = idNo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdNo() {
        return idNo;
    }

    public String getName() {
        return name;
    }
}
