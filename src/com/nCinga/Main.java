package com.nCinga;

import com.nCinga.bo.*;

public class Main {

    public static void main(String[] args) {

        Book[] books = new Book[2];
        
        books[0] = new Book("Math Book", Subject.MATHS,10,2);
        books[1] = new Book("BioBook", Subject.BIO,8,5);
        
        Book book = new Book("BioBook", Subject.BIO,8,5);
        Student student = new Student(111,"yasas","13","section B",Degree.B_TECH,5);
        Admin admin = new Admin("Admin Sali",202);
        IssuedBook issuedBook = null;
        
        Library library = new Library(books);
        
        library.issueBookService(student,admin,book,issuedBook);

    }


}
