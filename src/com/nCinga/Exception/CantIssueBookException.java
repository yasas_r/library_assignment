package com.nCinga.Exception;

public class CantIssueBookException extends RuntimeException {

        public CantIssueBookException(String message){
            super(message);
        }
}
