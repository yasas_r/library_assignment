package com.nCinga.Exception;

public class CantReachMoreThanTotalException extends CantIssuedDegreeException {
    public CantReachMoreThanTotalException() {
        super("The book count can’t reach more than the total count of the book.");
    }
}
