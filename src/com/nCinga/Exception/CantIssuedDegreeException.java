package com.nCinga.Exception;

public class CantIssuedDegreeException extends CantIssueBookException {
    public CantIssuedDegreeException(String message){
        super(message);
    }

}
